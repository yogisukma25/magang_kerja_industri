@extends('layouts.main')
@section('content')

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="{{ route('ref_kata_pembuka.create') }}" class="btn btn-primary mb-3">Tambah</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Kategori</th>
                        <th scope="col">Kata Pembuka</th>
                        <th scope="col">Aksi</th>
                    </tr>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $row)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $row->kategoris->name}}</td>
                            <td>{{ $row->kata_pembuka }}</td>
                            <td>
                                <a href="{{ route('ref_kata_pembuka.edit',  Crypt::encryptString($row->id)) }}" class="btn btn-warning">
                                    <i class="fas fa-edit"></i> <!-- Font Awesome Edit Icon -->
                                </a>
                                
                                <form id="delete" action="{{ route('ref_kata_pembuka.destroy', $row) }}" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">
                                        <i class="fas fa-trash-alt"></i> <!-- Font Awesome Trash Icon -->
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
