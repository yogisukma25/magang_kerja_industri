@extends('layouts.main')
@section('content')

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="{{ route('user.create') }}" class="btn btn-primary mb-3">Tambah</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama Depan</th>
                        <th scope="col">Nama Belakang</th>
                        <th scope="col">Email</th>
                        <th scope="col">Nomor Personal</th>
                        <th scope="col">Nomor Bisnis</th>
                        @if (Auth::user()->hasRole(['super-admin']))
                        <th scope="col">Aksi</th>
                        @endif
                    </tr>
                    </tr>
                </thead>
                <tbody>
                    @foreach($user as $key => $row)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $row->first_name }}</td>
                            <td>{{ $row->last_name }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->no_personal }}</td>
                            <td>{{ $row->no_bisnis }}</td>
                            @if(Auth::user()->hasRole('super-admin'))
                            <td>
                                <a href="{{ route('user.edit', Crypt::encryptString($row->id)) }}" class="btn btn-warning">
                                    <i class="fas fa-edit"></i> <!-- Font Awesome Edit Icon -->
                                </a>
                                
                                <form id="delete" action="{{ route('user.destroy', $row->id) }}" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger" @if(auth()->user()->id == $row->id) disabled @endif>
                                        <i class="fas fa-trash-alt"></i> <!-- Font Awesome Trash Icon -->
                                    </button>
                                </form>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
