@extends('layouts.main')
@section('content')

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="{{ route('artikel.create') }}" class="btn btn-primary mb-3">Tambah</a>
        <a href="{{ route('artikel.export') }}" class="btn btn-success mb-3">Export</a>
        <a href="{{ route('artikel.clear') }}" class="btn btn-danger mb-3">Clear</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Artikel</th>
                    </tr>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $row)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $row->judul }}</td>
                            <td>{!!$row->artikel!!}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
