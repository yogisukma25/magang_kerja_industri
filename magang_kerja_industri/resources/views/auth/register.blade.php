@extends('layouts.app')
@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
            <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
            <div class="col-lg-7">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Buat akun!</h1>
                    </div>
                    <form action="{{ route ('register') }}" method="post" class="user">
                        @csrf
                        <div class="row form-group">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control form-control-user @error('first_name') is-invalid @enderror" id="first_name" name="first_name"
                                    placeholder="Masukkan Nama Depan" value="{{ old('first_name') }}">
                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control form-control-user @error('last_name') is-invalid @enderror" id="last_name" name="last_name"
                                    placeholder="Masukkan Nama Belakang" value="{{ old('last_name') }}">
                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control form-control-user @error('email') is-invalid @enderror" id="email" name="email"
                                placeholder="Masukkan Alamat Email" value="{{ old('email') }}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control form-control-user @error('no_personal') is-invalid @enderror"
                                    id="no_personal" name="no_personal" placeholder="Masukkan Nomor Telepon Personal" value="{{ old('no_personal') }}">
                                @error('no_personal')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-user"
                                    id="no_bisnis" placeholder="Masukkan Nomor Telepon Bisnis" name="no_bisnis" value="{{ old('no_bisnis') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <select class="form-control @error('kategori_bisnis_id') is-invalid @enderror" id="kategori_bisnis_id" name="kategori_bisnis_id" style="border-radius:25px;height:50px">
                                <option selected disabled>Pilih Kategori Bisnis ..</option>
                                @foreach ($kategori_bisnis as $bisnis)
                                    <option value="{{ $bisnis->id }}" @if(old('kategori_bisnis_id') == $bisnis->id) selected @endif>{{ $bisnis->name }}</option>
                                @endforeach
                            </select>                                                    
                            @error('kategori_bisnis_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                             @enderror
                        </div>
                        <div class="form-group">
                            <div class="mb-3 mb-sm-0">
                                <input type="text" class="form-control form-control-user @error('alamat') is-invalid @enderror" id="alamat" name="alamat"
                                    placeholder="Masukkan Alamat" value="{{ old('alamat') }}">
                                @error('alamat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3 mb-sm-0">
                                <input type="text" class="form-control form-control-user @error('link_website') is-invalid @enderror" id="link_website" name="link_website"
                                    placeholder="Masukkan Link Website" value="{{ old('link_Website') }}">
                                @error('link_website')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror"
                                    id="password" name="password" placeholder="Masukkan Password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control form-control-user"
                                    id="password-confirm" placeholder="Ulangi Password" name="password_confirmation">
                            </div>
                        </div>
                        <button class="btn btn-primary btn-user btn-block" type="submit">
                            Register Account
                        </button>
                    </form>
                    <hr>
                    <div class="text-center">
                        <a class="small" href="{{ route('password.request') }}">
                            {{ __('Lupa password anda?') }}
                        </a>
                    </div>
                    <div class="text-center">
                        <a class="small" href="{{ route ('login') }}">Sudah punya akun? Login!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
