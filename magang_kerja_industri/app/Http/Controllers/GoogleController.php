<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Facades\JWTAuth;

class GoogleController extends Controller
{
    /**
     * Method redirectToGoogle
     *
     * @return void
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Method handleGoogleCallback
     * Fungsi login dengan google login
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
            $finduser = User::where('email', $user->email)->first();

            if ($finduser) {
                $token = JWTAuth::fromUser($finduser);
                Auth::login($finduser);

                if ($finduser->hasRole('contributor')) {
                    return redirect('/dashboard')->header('Authorization', 'Bearer ' . $token);
                } elseif ($finduser->hasRole('contributor-pro')) {
                    return redirect('/dashboard')->header('Authorization', 'Bearer ' . $token);
                }
            } else {
                return redirect()->route('login')->withErrors(['email' => 'Akun Google tidak terhubung dengan akun kami.']);
            }
        } catch (Exception $e) {
            return redirect()->route('login')->withErrors(['email' => 'Akun Google tidak terhubung dengan akun kami.']);
        }
    }

}
