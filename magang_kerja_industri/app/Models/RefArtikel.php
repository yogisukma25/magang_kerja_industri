<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefArtikel extends Model
{
    use HasFactory;
    protected $table = 'ref_artikel';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kategori',
        'isi_artikel',
    ];
}
